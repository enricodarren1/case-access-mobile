import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-input-address',
  templateUrl: './input-address.component.html',
  styleUrls: ['./input-address.component.scss']
})
export class InputAddressComponent implements OnInit {

  @Output() dataInput = new EventEmitter();

  label = "Dimanakah alamat tempat tinggal anda?"
  inputForm: FormGroup;

  constructor(
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.inputForm = this.fb.group({
      address: new FormControl('', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(50)])),
    });
  }

  onSubmit() {
    this.dataInput.emit({index: 5, question: this.label, value: this.inputForm.value.address})
  }

}
