import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-input-phone-number',
  templateUrl: './input-phone-number.component.html',
  styleUrls: ['./input-phone-number.component.scss']
})
export class InputPhoneNumberComponent implements OnInit {

  @Output() dataInput = new EventEmitter();

  label = "Berapa nomor telpon anda?";
  inputForm: FormGroup;

  constructor(
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.inputForm = this.fb.group({
      phoneNumber: new FormControl('', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(200)])),
    })
  }

  onSubmit() {
    this.dataInput.emit({index: 4, question: this.label, value: this.inputForm.value.phoneNumber});
  }

}
