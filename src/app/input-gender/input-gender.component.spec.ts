import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputGenderComponent } from './input-gender.component';

describe('InputGenderComponent', () => {
  let component: InputGenderComponent;
  let fixture: ComponentFixture<InputGenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputGenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputGenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
