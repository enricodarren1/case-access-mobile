import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'testcase-access-mobile';

  formValue: any = [];
  index = 0;

  getDataInputName(ev) {
    this.index = ev.index;
    this.formValue.push(ev);
  }

  getDataInputEmail(ev) {
    this.index = ev.index;
    this.formValue.push(ev);
  }

  getDataInputGender(ev) {
    this.index = ev.index;
    this.formValue.push(ev);
  }

  getDataInputPhoneNumber(ev) {
    this.index = ev.index;
    this.formValue.push(ev);
  }

  getDataInputAddress(ev) {
    this.index = ev.index;
    this.formValue.push(ev);
  }
}
