import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-input-name',
  templateUrl: './input-name.component.html',
  styleUrls: ['./input-name.component.scss']
})
export class InputNameComponent implements OnInit {

  @Output() dataInput = new EventEmitter();

  label = "Siapakah nama lengkap anda?";
  inputForm: FormGroup;

  constructor(
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.inputForm = this.fb.group({
      name: new FormControl('', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(50)])),
    })
  }

  onSubmit() {
    this.dataInput.emit({index: 1, question: this.label, value: this.inputForm.value.name});
  }

}
