import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-input-gender',
  templateUrl: './input-gender.component.html',
  styleUrls: ['./input-gender.component.scss']
})
export class InputGenderComponent implements OnInit {

  @Output() dataInput = new EventEmitter();

  label = "Apakah jenis kelamin anda?";
  inputForm: FormGroup;

  constructor(
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.inputForm = this.fb.group({
      gender: new FormControl('', Validators.required),
    })
  }

  onSubmit() {
    this.dataInput.emit({index: 3, question: this.label, value: this.inputForm.value.gender});
  }

}
